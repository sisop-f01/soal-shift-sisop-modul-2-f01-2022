#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <json-c/json.h>

char* create_file();
char* create_folder();
int download();
int initdir();
int initfile();
char* int_to_string();
char* map_to_character();
char* map_to_weapon();
void move();
void print_to_file();
int rmzip();
int unzip();
void zip();

char* create_file(char* curr_dir, int n, int balance, int gacha_cost, int hour, int minute, int second){
      int gacha_left = balance / gacha_cost;
    //   printf("%d\n", gacha_left);
      if(gacha_left > 10) gacha_left = 10;
      gacha_left += n - 1;
      
      char* curr_file = (char*) calloc(200, sizeof(char));
      strcpy(curr_file, curr_dir);
      strcat(curr_file, "/");
      if(hour < 10){strcat(curr_file, "0");}
      strcat(curr_file, int_to_string(hour));
      strcat(curr_file, ":");
      if(minute < 10){strcat(curr_file, "0");}
      strcat(curr_file, int_to_string(minute));
      strcat(curr_file, ":");
      if(second < 10){strcat(curr_file, "0");}
      strcat(curr_file, int_to_string(second));
      strcat(curr_file, ":");
      strcat(curr_file, "_gacha_");
      strcat(curr_file, int_to_string(gacha_left));
      strcat(curr_file, ".txt");
    //   printf("%s\n", curr_file);
      initfile(curr_file);
      return curr_file;
}

char* create_folder(char* dir, int n, int balance, int gacha_cost){

    int gacha_left = balance / gacha_cost;
    // printf("%d\n", gacha_left);
    if(gacha_left > 90) gacha_left = 90;
    gacha_left += n - 1;

    char* curr_dir = (char*) calloc(200, sizeof(char));
    strcpy(curr_dir, dir);
    strcat(curr_dir, "total_gacha_");
    strcat(curr_dir, int_to_string(gacha_left));
    // printf("%s\n", curr_dir);
    initdir(curr_dir);
    return curr_dir;
}

int download(char link[], char output[]){
    int pid = fork();
    if(pid == -1){
        return -1;
    }else{
        if(!pid){
            if(execlp("/usr/bin/wget",
            "wget", 
            link, 
            "-O", 
            output, 
            "-q",
            NULL) == -1) return -3;
        }else{
            wait(NULL);
        }
    }
    return 0;
}

int initdir(char dir[]){
    int pid = fork();
    if(pid == -1){
        return -1;
    }else{
        if(!pid){
            if(execlp("/usr/bin/mkdir", 
            "mkdir", 
            "-p", 
            dir, 
            NULL) == -1) return -2; 
        }else{
            wait(NULL);
        }
    }
    return 0;
}

int initfile(char file[]){
    int pid = fork();
    if(pid == -1) return -1;
    else{
        if(!pid){
            if(execlp(
                "/usr/bin/touch",
                "touch",
                file,
                NULL
            ) == -1) return -6;
        }else{
            wait(NULL);
        }
    }
    return 0;
}

char* int_to_string(int x){
    int digits = 0;
    int y = x;
    while(1){
        y /= 10;
        digits++;
        if(!y) break;
    }
    // printf("digits %d\n", digits);

    char* str = (char*)calloc(digits + 1, sizeof(char));

    if(str == NULL){
        printf("MALLOC ERROR");
        return NULL;
    }else{
        int i = 0;
        for(i; i < digits; i++){
            str[i] = '7';
        }str[i] = '\0';
        int len = (int) (sizeof(str) / sizeof(char));

        i = 0;
        while(--digits >= 0){
            y = x / (int) pow((double) 10, (double) digits);
            // printf("%d\n", y);
            x %= (int) pow(10, digits);
            char a = (char) y + '0';
            str[i++] = a;
        }return str;
    }
}

char* map_to_character(int key){
    switch(key){
        case 1:
            return "aether";
        case 2:
            return "albedo";
        case 3:
            return "aloy";
        case 4:
            return "amber";
        case 5:
            return "aratakiitto";
        case 6:
            return "barbara";
        case 7:
            return "beidou";
        case 8:
            return "bennett";
        case 9:
            return "chongyun";
        case 10:
            return "diluc";
        case 11:
            return "diona";
        case 12:
            return "eula";
        case 13:
            return "fischl";
        case 14:
            return "ganyu";
        case 15:
            return "gorou";
        case 16:
            return "hutao";
        case 17:
            return "jean";
        case 18:
            return "kaedeharakazuha";
        case 19:
            return "kaeya";
        case 20:
            return "kamisatoayaka";
        case 21:
            return "keqing";
        case 22:
            return "klee";
        case 23:
            return "kujousara";
        case 24:
            return "lisa";
        case 25:
            return "lumine";
        case 26:
            return "mona";
        case 27:
            return "ningguang";
        case 28:
            return "noelle";
        case 29:
            return "qiqi";
        case 30:
            return "raidenshogun";
        case 31:
            return "razor";
        case 32:
            return "rosaria";
        case 33:
            return "sangonomiyakokomi";
        case 34:
            return "sayu";
        case 35:
            return "shenhe";
        case 36:
            return "sucrose";
        case 37:
            return "tartaglia";
        case 38:
            return "thoma";
        case 39:
            return "venti";
        case 40:
            return "xiangling";
        case 41:
            return "xiao";
        case 42:
            return "xingqiu";
        case 43:
            return "xinyan";
        case 44:
            return "yaemiko";
        case 45:
            return "yanfei";
        case 46:
            return "yoimiya";
        case 47:
            return "yunjin";
        case 48:
            return "zhongli";
        default:
            return "KEY IS OUT OF RANGE";
    }
}

char* map_to_weapon(int key){
    switch(key){
        case 1:
            return "akuoumaru";
        case 2:
            return "alleyhunter";
        case 3:
            return "amenomakageuchi";
        case 4:
            return "amosbow";
        case 5:
            return "apprenticesnotes";
        case 6:
            return "aquilafavonia";
        case 7:
            return "beginnersprotector";
        case 8:
            return "blackcliffagate";
        case 9:
            return "blackclifflongsword";
        case 10:
            return "blackcliffpole";
        case 11:
            return "blackcliffslasher";
        case 12:
            return "blackcliffwarbow";
        case 13:
            return "blacktassel";
        case 14:
            return "bloodtaintedgreatsword";
        case 15:
            return "calamityqueller";
        case 16:
            return "cinnabarspindle";
        case 17:
            return "compoundbow";
        case 18:
            return "coolsteel";
        case 19:
            return "crescentpike";
        case 20:
            return "darkironsword";
        case 21:
            return "deathmatch";
        case 22:
            return "debateclub";
        case 23:
            return "dodocotales";
        case 24:
            return "dragonsbane";
        case 25:
            return "dragonspinespear";
        case 26:
            return "dullblade";
        case 27:
            return "elegyfortheend";
        case 28:
            return "emeraldorb";
        case 29:
            return "engulfinglightning";
        case 30:
            return "everlastingmoonglow";
        case 31:
            return "eyeofperception";
        case 32:
            return "favoniuscodex";
        case 33:
            return "favoniusgreatsword";
        case 34:
            return "favoniuslance";
        case 35:
            return "favoniussword";
        case 36:
            return "favoniuswarbow";
        case 37:
            return "ferrousshadow";
        case 38:
            return "festeringdesire";
        case 39:
            return "filletblade";
        case 40:
            return "freedomsworn";
        case 41:
            return "frostbearer";
        case 42:
            return "hakushinring";
        case 43:
            return "halberd";
        case 44:
            return "hamayumi";
        case 45:
            return "harbingerofdawn";
        case 46:
            return "huntersbow";
        case 47:
            return "ironpoint";
        case 48:
            return "ironsting";
        case 49:
            return "kagurasverity";
        case 50:
            return "katsuragikirinagamasa";
        case 51:
            return "kitaincrossspear";
        case 52:
            return "lionsroar";
        case 53:
            return "lithicblade";
        case 54:
            return "lithicspear";
        case 55:
            return "lostprayertothesacredwinds";
        case 56:
            return "luxurioussealord";
        case 57:
            return "magicguide";
        case 58:
            return "mappamare";
        case 59:
            return "memoryofdust";
        case 60:
            return "messenger";
        case 61:
            return "mistsplitterreforged";
        case 62:
            return "mitternachtswaltz";
        case 63:
            return "mouunsmoon";
        case 64:
            return "oathsworneye";
        case 65:
            return "oldmercspal";
        case 66:
            return "otherworldlystory";
        case 67:
            return "pocketgrimoire";
        case 68:
            return "polarstar";
        case 69:
            return "predator";
        case 70:
            return "primordialjadecutter";
        case 71:
            return "primordialjadewingedspear";
        case 72:
            return "prototypeamber";
        case 73:
            return "prototypearchaic";
        case 74:
            return "prototypecrescent";
        case 75:
            return "prototyperancour";
        case 76:
            return "prototypestarglitter";
        case 77:
            return "rainslasher";
        case 78:
            return "ravenbow";
        case 79:
            return "recurvebow";
        case 80:
            return "redhornstonethresher";
        case 81:
            return "royalbow";
        case 82:
            return "royalgreatsword";
        case 83:
            return "royalgrimoire";
        case 84:
            return "royallongsword";
        case 85:
            return "royalspear";
        case 86:
            return "rust";
        case 87:
            return "sacrificialbow";
        case 88:
            return "sacrificialfragments";
        case 89:
            return "sacrificialgreatsword";
        case 90:
            return "sacrificialsword";
        case 91:
            return "seasonedhuntersbow";
        case 92:
            return "serpentspine";
        case 93:
            return "sharpshootersoath";
        case 94:
            return "silversword";
        case 95:
            return "skyridergreatsword";
        case 96:
            return "skyridersword";
        case 97:
            return "skywardatlas";
        case 98:
            return "skywardblade";
        case 99:
            return "skywardharp";
        case 100:
            return "skywardpride";
        case 101:
            return "skywardspine";
        case 102:
            return "slingshot";
        case 103:
            return "snowtombedstarsilver";
        case 104:
            return "solarpearl";
        case 105:
            return "songofbrokenpines";
        case 106:
            return "staffofhoma";
        case 107:
            return "summitshaper";
        case 108:
            return "swordofdescension";
        case 109:
            return "thealleyflash";
        case 110:
            return "thebell";
        case 111:
            return "theblacksword";
        case 112:
            return "thecatch";
        case 113:
            return "theflute";
        case 114:
            return "thestringless";
        case 115:
            return "theunforged";
        case 116:
            return "theviridescenthunt";
        case 117:
            return "thewidsith";
        case 118:
            return "thrillingtalesofdragonslayers";
        case 119:
            return "thunderingpulse";
        case 120:
            return "travelershandysword";
        case 121:
            return "twinnephrite";
        case 122:
            return "vortexvanquisher";
        case 123:
            return "wastergreatsword";
        case 124:
            return "wavebreakersfin";
        case 125:
            return "whiteblind";
        case 126:
            return "whiteirongreatsword";
        case 127:
            return "whitetassel";
        case 128:
            return "windblumeode";
        case 129:
            return "wineandsong";
        case 130:
            return "wolfsgravestone";
        default:
            return "KEY IS OUT OF RANGE";
    }
}

void move(char src[], char dest[]){
    int pid = fork();
    if(pid == -1) exit(-1);
    else{
        if(!pid){
            execlp(
                "/usr/bin/mv",
                "mv",
                src,
                dest,
                NULL
            );
        }else{
            wait(NULL);
        }
    }
}

void print_to_file(char filepath[], int n, const char* rarity, const char* name, int balance){
    FILE *fptr;
    fptr = fopen(filepath, "a");
    if(fptr == NULL) exit(-7);
    
    char type[12];
    if(n % 2 == 0) strcpy(type, "weapons");
    else strcpy(type, "characters");
    fprintf(fptr,"%d_%s_%s_%s_%d\n", n, type, rarity, name, balance);
    fclose(fptr);
}

int rmzip(char dir[]){
    int pid = fork();
    if(pid == -1) return -1;
    else{
        if(!pid){
            if(execlp(
                "/usr/bin/rm",
                "rm",
                "-r",
                "-f",
                dir,
                NULL
            ) == -1) return -5;
        }
        else{wait(NULL);}
    }return 0;
}

int unzip(char zip[], char dest[]){
    int pid = fork();
    if(pid == -1){
        return -1;
    }
    else{
        if(!pid){
            if(execlp("/usr/bin/unzip", 
            "unzip",
            "-q",
            zip,
            "-d",
            dest,
            NULL) == -1) return -4;
        }else{
            wait(NULL);
        }
    }
    return 0;
}

void zip(char file[], char output[], char password[]){
    int pid = fork();
    if(pid == -1){
        exit(-1);
    }else{
        if(!pid){
            execlp(
                "/usr/bin/zip",
                "zip",
                "--password",
                password,
                "-m",
                "-r",
                output,
                file,
                NULL
            );
        }else{
            wait(NULL);
        }
    }
}

int main(){
    
    int pid = fork();
    if(pid == -1){
        exit(1);
    }else{
        if(pid){
            exit(0);
        }
    }

    int sid = setsid();
    if(sid == -1){
        exit(1);
    }

    chdir("/");
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    time_t ttime;
    struct tm *time_struct;

    time(&ttime);
    time_struct = gmtime(&ttime);

    int month = time_struct->tm_mon;
    int date = (time_struct->tm_hour + 7)/24 + time_struct->tm_mday;
    int hour = (time_struct->tm_hour + 7) % 24;
    int minute = time_struct->tm_min;
    int second = time_struct->tm_sec;

    FILE *fp = fopen("/home/nfpurnama/soal1_log.txt", "w+");

    // printf(" m-%d\n d-%d\n h-%d\n min-%d\n s-%d\n\n", month, date, hour, minute, second);

    srand(time(NULL));

    while(1){
        sleep(1);

        time(&ttime);
        time_struct = gmtime(&ttime);

        month = time_struct->tm_mon;
        date = (time_struct->tm_hour + 7)/24 + time_struct->tm_mday;
        hour = (time_struct->tm_hour + 7) % 24;
        minute = time_struct->tm_min;
        second = time_struct->tm_sec;

        if(!(date-30) && !(month-2) && !(hour-4) && !(minute-44) && !second){
            if(initdir("/home/nfpurnama/gacha_gacha") < 0) return 1;
            fprintf(fp, "created directory\n");
            fflush(fp);
            if(download(
            "https://docs.google.com/uc?export=download\\&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp",
            "/home/nfpurnama/c.zip"
            ) < 0) return 2;
            fprintf(fp, "downloaded characters\n");
            fflush(fp);
            if(download(
            "https://docs.google.com/uc?export=download\\&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT",
            "/home/nfpurnama/i.zip"
            ) < 0) return 2;
            fprintf(fp, "downloaded weapons\n");
            fflush(fp);

            if(unzip("/home/nfpurnama/c.zip", "/home/nfpurnama") < 0) return 3;
            fprintf(fp, "unzipped characters\n");
            fflush(fp);
            if(unzip("/home/nfpurnama/i.zip", "home/nfpurnama") < 0) return 3;
            fprintf(fp, "unzipped weapons\n");
            fflush(fp);

            if(rmzip("/home/nfpurnama/c.zip") < 0) return 4;
            fprintf(fp, "removed characters\n");
            fflush(fp);

            if(rmzip("/home/nfpurnama/i.zip") < 0) return 4;    
            fprintf(fp, "removed weapons\n");
            fflush(fp);


            int n = 0;
            int gacha_cost = 160;

            char dir[] = "/home/nfpurnama/gacha_gacha/";
            char curr_dir[200];
            char curr_file[200];

        // date == 30 && month == 2 && hour == 4 && minute == 44        
            int gacha_left;
            

            for(int balance = 79000; balance / gacha_cost > 0; balance -= gacha_cost){
                // printf("gacha...\n");
                
                n++;
                if(n % 90 == 1){
                    strcpy(curr_dir, create_folder(dir, n, balance, gacha_cost));
                    fprintf(fp, "gacha'd mod 90\n");
                    fflush(fp);

                    // printf("current directory %s\n", curr_dir);
                } //make new folder total_gacha_{n}
                if(n % 10 == 1){
                    strcpy(curr_file, create_file(curr_dir, n,  balance, gacha_cost, hour, minute, second));
                    fprintf(fp, "gacha'd mod 10\n");
                    fflush(fp);

                    // printf("current file %s\n", curr_file);
                } //make new output file HHMMSS_gacha_{n}

                char gacha_file[200];

                if(n % 2 == 0){
                    int res = rand() % 130 + 1;
                    // printf("item gacha %d\n", res);
                    strcpy(gacha_file, "/home/nfpurnama/weapons/");
                    strcat(gacha_file, map_to_weapon(res));
                    strcat(gacha_file, ".json");

                    fprintf(fp, "gachaaaa weapons\n");
                    fflush(fp);

                } //item gacha
                else{
                    int res = rand() % 48 + 1;
                    // printf("character gacha %d\n", res);
                    strcpy(gacha_file, "/home/nfpurnama/characters/");
                    strcat(gacha_file, map_to_character(res));  
                    strcat(gacha_file, ".json");          
                
                    fprintf(fp, "gachaaaa characters\n");
                    fflush(fp);

                } //character gacha

                FILE *fptr;
                char buffer[5192];

                struct json_object *parsed_json;
                struct json_object *gacha_name;
                struct json_object *gacha_rarity;

                fptr = fopen(gacha_file, "r");
                fread(buffer, 5192, 1, fptr);
                fclose(fptr);

                parsed_json = json_tokener_parse(buffer);
                json_object_object_get_ex(parsed_json, "name", &gacha_name);
                json_object_object_get_ex(parsed_json, "rarity", &gacha_rarity);

                // printf("\nname: %s\n", json_object_get_string(gacha_name));
                // printf("rarity: %s\n", json_object_get_string(gacha_rarity));

                second++;
                if(second == 60){
                    second = 0;
                    minute++;
                }if(minute == 60){
                    minute = 0;
                    hour++;
                }
                // printf("gacha is %s\n", gacha_file);
                print_to_file(curr_file, n, json_object_get_string(gacha_rarity), json_object_get_string(gacha_name), balance-gacha_cost);
                // printf("testing for loop...\n");
            }  
            
            // sleep(3*60*60);     
        }
        
        if(!(date-30) && !(month-2) && !(hour-7) && !(minute-44) && !(second)){
            zip("/home/nfpurnama/gacha_gacha", "/home/nfpurnama/not_safe_for_wibu", "satuduatiga");
            if(rmzip("/home/nfpurnama/characters") < 0) return 4;
            if(rmzip("/home/nfpurnama/weapons") < 0) return 4;
            break;
        }
    }

    fclose(fp);
    
    return 0;
}

