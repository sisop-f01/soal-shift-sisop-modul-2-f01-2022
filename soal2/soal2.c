#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <dirent.h>
#include <malloc.h>

struct drakor{
    char name[100];
    char year[10];
}drk[100];


size_t listFile(const char *path, char ***ls){
	size_t count = 0;
	size_t length = 0;
	DIR *dp = NULL;
	struct dirent *ep = NULL;

	dp = opendir(path);
	if(NULL == dp){
		fprintf(stderr, "No such directory %s", path);
		return 0;
	}

	*ls = NULL;
	ep = readdir(dp);
	while(NULL != ep){
		count++;
		ep = readdir(dp);
	}

	rewinddir(dp);
	*ls = calloc(count, sizeof(char *));

	count = 0;
	ep = readdir(dp);
	while(NULL != ep){
		if(!strcmp(ep->d_name, ".") == 0 || !strcmp(ep->d_name, "..") == 0){
			(*ls)[count++] = strdup(ep->d_name);
                        ep = readdir(dp);
		}
		else{
			break;
		}
	}
	closedir(dp);
	return count;
}

int dtmsplit(char *str, const char *delim, char ***array, int *length){
	int i=0;
	char *token;
	char **res = (char **)malloc(0 * sizeof(char *));

	//get first token
	token = strtok(str, delim);
	while(token != NULL){
		res = (char **)realloc(res, (i+1) * sizeof(char *));
		res[i] = token;
		i++;
		token = strtok(NULL, delim);
	}
	*array = res;
	*length = i;
	return 1;
}


int main() {
	DIR *dp;
	struct dirent *ep;
	char path[100] = "/home/azzuraf/shift2/drakor";

	FILE *f;
	pid_t child_id;
    int status;

    child_id = fork();

    if(child_id == 0){
            char *arg[] = {"mkdir","-p","/home/azzuraf/shift2/drakor", NULL};
            execv("/usr/bin/mkdir", arg);
	}
    else {
		while((wait(&status)) > 0);
		child_id = fork();

		if(child_id == 0){
			char *argv[] = {"unzip","/home/azzuraf/Downloads/drakor.zip", "*.png","-d", "/home/azzuraf/shift2/drakor",NULL};
	        execv("/usr/bin/unzip", argv);
		}else{
			while((wait(&status)) > 0);

	        char **files;
       	    size_t count;

        	count = listFile("/home/azzuraf/shift2/drakor", &files);

            for(int i=0; i < count; i++){
       			//copying same file
        	    char fileUntouched[70];
                strcpy(fileUntouched, files[i]);

       	        //removing extension
        	    files[i][strlen(files[i]) - 4] = '\0';

        	    int c = 0;
                int countTok = 0;
               	char **indivDrakor = NULL;

       	        //counting each drakor
	         	c = dtmsplit(files[i], "_", &indivDrakor, &countTok);

                for(int j=0; j < countTok; j++){
       	        	int d = 0;
       		    	int drakorAtrTok = 0;
                	char **drakorAtr = NULL;

                	char temp[100];
                	strcpy(temp, indivDrakor[j]);

                	d = dtmsplit(temp, ";", &drakorAtr, &drakorAtrTok);

					char genre[100];
               		sprintf(genre, "/home/azzuraf/shift2/drakor/%s", drakorAtr[2]);
               	              	
               		int child_id_mkdir;
					int status_mkdir;

                	child_id_mkdir = fork();

					if(child_id_mkdir == 0){
						if(strcmp(drakorAtr[2], "(null)") != 0){
                        	char *arg[] = {"mkdir", "-p",genre, NULL};
                        	execv("/usr/bin/mkdir", arg);
						}
       		    	}
					else{
						while ((wait(&status_mkdir) > 0));

						child_id = fork();

						if(child_id == 0){
							char dataPath[100];
							sprintf(dataPath, "/home/azzuraf/shift2/drakor/%s/data.txt", drakorAtr[2]);
							
							char name[100];
							sprintf(name,"%s", drakorAtr[0]);
							char year[10];
							sprintf(year,"%s", drakorAtr[1]);
							
							f = fopen(dataPath, "a");
							fprintf(f, "%s\n", name);
                        	fprintf(f, "%s\n", year);
                        	fclose(f);
							
							char fromFile[100], toFile[100];
							sprintf(fromFile, "%s", fileUntouched);
							sprintf(toFile, "../drakor/%s/%s.png", drakorAtr[2], drakorAtr[0]);

							chdir("/home/azzuraf/shift2/drakor/");

							char *arg[] = {"cp", fromFile, toFile, NULL};
							execv("/bin/cp", arg);
						}
					}
            	}
        	}
		}
	}
	
	dp = opendir(path);
    if (dp != NULL){
        while((ep = readdir(dp))){
            if (ep->d_type == 4){
                if (strstr(ep->d_name, "..") != NULL) continue;
                if (strstr(ep->d_name, ".") != NULL) continue;
                char dataPath[100];
                char ch;
                char name[100];
                char year[100];
                strcpy(dataPath, path);
                strcat(dataPath, "/");
                strcat(dataPath, ep->d_name);
                strcat(dataPath, "/data.txt");

                FILE *readFile;
                readFile = fopen(dataPath, "r");

                int index = 0;
                int line = 1; 
                while(!feof(readFile)){
                    if (line%2 == 1){
                        fscanf(readFile, "%s", drk[index].name);
                    }
                    else{
                        fscanf(readFile, "%s", drk[index].year);
                        index++;
                    }
                    line++;
                }

                for (int i = 0; i<index-1; i++){
                    for (int j = 0; j<index-i-1 ; j++){
                        int y1 = atoi(drk[j].year);
                        int y2 = atoi(drk[j+1].year);
                        if (y1 > y2){
                            char temp[100];
                            strcpy(temp, drk[j].year);
                            strcpy(drk[j].year, drk[j+1].year);
                            strcpy(drk[j+1].year, temp);

                            strcpy(temp, drk[j].name);
                            strcpy(drk[j].name, drk[j+1].name);
                            strcpy(drk[j+1].name, temp);
                        }
                    }
                }

                FILE *writeFile;
                writeFile = fopen(dataPath, "w");
                fclose(writeFile);
                writeFile = fopen(dataPath, "a");

                fprintf(writeFile, "Kategori: %s\n", ep->d_name);

                for (int i = 0; i<index; i++){
                    fprintf(writeFile, "\nNama : %s\n", drk[i].name);
                    fprintf(writeFile, "Tahun : %s\n", drk[i].year);

                }

                fclose(readFile);
                fclose(writeFile);
            }
        }
    }
        
    dp = opendir(path);
    if (dp != NULL){
        while((ep = readdir(dp))){
            if (ep->d_type == 4) continue;

            char filePath[100];

            strcpy(filePath, "/home/azzuraf/shift2/drakor/");
            strcat(filePath, ep->d_name);

            remove(filePath);
        }
    }
}
