# Soal Shift Modul 2

Anggota Kelompok F01:
1. Angela Oryza Prabowo (5025201022)
1. Azzura Ferliani Ramadhani (5025201190)
1. Naufal Adli Purnama (5025201195)

## Soal 1
Mas Refadi adalah seorang wibu gemink.  Dan jelas game favoritnya adalah bengshin impek. Terlebih pada game tersebut ada sistem gacha item yang membuat orang-orang selalu ketagihan untuk terus melakukan nya. Tidak terkecuali dengan mas Refadi sendiri. Karena rasa penasaran bagaimana sistem gacha bekerja, maka dia ingin membuat sebuah program untuk men-simulasi sistem history gacha item pada game tersebut. Tetapi karena dia lebih suka nge-wibu dibanding ngoding, maka dia meminta bantuanmu untuk membuatkan program nya. Sebagai seorang programmer handal, bantulah mas Refadi untuk memenuhi keinginan nya itu. 

- `a.` Saat program pertama kali berjalan. Program akan mendownload file characters dan file weapons dari link yang ada dibawah, lalu program akan mengekstrak kedua file tersebut. File tersebut akan digunakan sebagai database untuk melakukan gacha item characters dan weapons. Kemudian akan dibuat sebuah folder dengan nama “gacha_gacha” sebagai working directory. Seluruh hasil gacha akan berada di dalam folder tersebut. Penjelasan sistem gacha ada di poin (d).

- `b.` Mas Refadi ingin agar setiap kali gacha, item characters dan item weapon akan selalu bergantian diambil datanya dari database. Maka untuk setiap kali jumlah-gacha nya bernilai genap akan dilakukan gacha item weapons, jika bernilai ganjil maka item characters. Lalu untuk setiap kali jumlah-gacha nya mod 10, maka akan dibuat sebuah file baru (.txt) dan output hasil gacha selanjutnya akan berada di dalam file baru tersebut. Dan setiap kali jumlah-gacha nya mod 90, maka akan dibuat sebuah folder baru dan file (.txt) selanjutnya akan berada didalam folder baru tersebut.  Sehingga untuk setiap folder, akan terdapat 9 file (.txt) yang didalamnya berisi 10 hasil gacha. Dan karena ini simulasi gacha, maka hasil gacha di dalam file .txt adalah ACAK/RANDOM dan setiap file (.txt) isi nya akan BERBEDA

- `c.` Format penamaan setiap file (.txt) nya adalah {Hh:Mm:Ss}_gacha_{jumlah-gacha}, misal 04:44:12_gacha_120.txt, dan format penamaan untuk setiap folder nya adalah total_gacha_{jumlah-gacha}, misal total_gacha_270. Dan untuk setiap file (.txt) akan memiliki perbedaan penamaan waktu output sebesar 1 second.

- `d.` Pada game tersebut, untuk melakukan gacha item kita harus menggunakan alat tukar yang dinamakan primogems. Satu kali gacha item akan menghabiskan primogems sebanyak 160 primogems. Karena mas Refadi ingin agar hasil simulasi gacha nya terlihat banyak, maka pada program, primogems di awal di-define sebanyak 79000 primogems. Setiap kali gacha, ada 2 properties yang akan diambil dari database, yaitu name dan rarity. Lalu Outpukan hasil gacha nya ke dalam file (.txt) dengan format hasil gacha {jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}. Program akan selalu melakukan gacha hingga primogems habis. 

Contoh : 157_characters_5_Albedo_53880

- `e.` Proses untuk melakukan gacha item akan dimulai bertepatan dengan anniversary pertama kali mas Refadi bermain bengshin impek, yaitu pada 30 Maret jam 04:44.  Kemudian agar hasil gacha nya tidak dilihat oleh teman kos nya, maka 3 jam setelah anniversary tersebut semua isi di folder gacha_gacha akan di zip dengan nama not_safe_for_wibu dengan dipassword "satuduatiga", lalu semua folder akan di delete sehingga hanya menyisakan file (.zip)



`Note:`
- Menggunakan fork dan exec.
- Tidak boleh menggunakan fungsi system(), mkdir(), dan rename().
- Tidak boleh pake cron.
- Semua poin dijalankan oleh 1 script di latar belakang. Cukup jalankan script 1x serta ubah time dan date untuk check hasilnya.

### Penyelesaian

- `a.` program harus mendownload dua file yang telah diberikan, lalu mengekstrak folder dari kedua file `.zip` tersebut. Untuk itu digunakan fungsi `execlp()` untuk menjalankan perintah `wget` dengan implementasi seperti berikut dalam fungsi `download`:

``` c
int download(char link[], char output[]){
    int pid = fork();
    if(pid == -1){
        return -1;
    }else{
        if(!pid){
            if(execlp("/usr/bin/wget",
            "wget", 
            link, 
            "-O", 
            output, 
            "-q",
            NULL) == -1) return -3;
        }else{
            wait(NULL);
        }
    }
    return 0;
}
```

Setelah itu, kedua file `.zip` akan diekstrak menggunakan fungsi `unzip()`:

``` c
int unzip(char zip[], char dest[]){
    int pid = fork();
    if(pid == -1){
        return -1;
    }
    else{
        if(!pid){
            if(execlp("/usr/bin/unzip", 
            "unzip",
            "-q",
            zip,
            "-d",
            dest,
            NULL) == -1) return -4;
        }else{
            wait(NULL);
        }
    }
    return 0;
}
```

Untuk membuat directory `gacha_gacha`, digunakan fungsi `initdir()`:

```c
int initdir(char dir[]){
    int pid = fork();
    if(pid == -1){
        return -1;
    }else{
        if(!pid){
            if(execlp("/usr/bin/mkdir", 
            "mkdir", 
            "-p", 
            dir, 
            NULL) == -1) return -2; 
        }else{
            wait(NULL);
        }
    }
    return 0;
}
```

- `b.` Karena setiap gacha bergantian, digunakan if statement seperti berikut untuk bergilir antara gacha character dan item: 

```c
if(n % 2 == 0){
    int res = rand() % 130 + 1;
	// printf("item gacha %d\n", res);
	strcpy(gacha_file, "/home/nfpurnama/weapons/");
	strcat(gacha_file, map_to_weapon(res));
	strcat(gacha_file, ".json");
} //item gacha
else{
	int res = rand() % 48 + 1;
	// printf("character gacha %d\n", res);
	strcpy(gacha_file, "/home/nfpurnama/characters/");
	strcat(gacha_file, map_to_character(res));  
	strcat(gacha_file, ".json");          
} //character gacha
```

fungsi `map_to_weapon()` dan fungsi `map_to_character()` berisi switch case yang menerima integer dan mengemalikan string berisi nama hasil gacha. Ini merupakan implementasi sistem gacha agar menghasilkan gacha yang acak.

Untuk membuat file baru setiap 10 gacha digunakan if statement berikut:

```c
if(n % 10 == 1){
    strcpy(curr_file, create_file(curr_dir, n,  balance, gacha_cost, hour, minute, second));
    // printf("current file %s\n", curr_file);
} //make new output file HHMMSS_gacha_{n}
```


Untuk membuat folder baru setiap 90 gacha digunakan if statement berikut:

```c
if(n % 90 == 1){
    strcpy(curr_dir, create_folder(dir, n, balance, gacha_cost));
    // printf("current directory %s\n", curr_dir);
} //make new folder total_gacha_{n}
```

- `c.` Untuk membuat file yang sesuai dengan ketentuan soal, dibuat fungsi `create_file()` seperti berikut:

```c
char* create_file(char* curr_dir, int n, int balance, int gacha_cost, int hour, int minute, int second){
      int gacha_left = balance / gacha_cost;
    //   printf("%d\n", gacha_left);
      if(gacha_left > 10) gacha_left = 10;
      gacha_left += n - 1;
      
      char* curr_file = (char*) calloc(200, sizeof(char));
      strcpy(curr_file, curr_dir);
      strcat(curr_file, "/");
      if(hour < 10){strcat(curr_file, "0");}
      strcat(curr_file, int_to_string(hour));
      strcat(curr_file, ":");
      if(minute < 10){strcat(curr_file, "0");}
      strcat(curr_file, int_to_string(minute));
      strcat(curr_file, ":");
      if(second < 10){strcat(curr_file, "0");}
      strcat(curr_file, int_to_string(second));
      strcat(curr_file, ":");
      strcat(curr_file, "_gacha_");
      strcat(curr_file, int_to_string(gacha_left));
      strcat(curr_file, ".txt");
    //   printf("%s\n", curr_file);
      initfile(curr_file);
      return curr_file;
}
```

Agar waktu pada setiap file memiliki interval 1 detik digunakan inkrementasi seperti berikut:

```c
second++;
if(second == 60){
	second = 0;
	minute++;
}if(minute == 60){
	minute = 0;
	hour++;
}
```

- `d.` Digunakan inisialisasi berikut sebagai definisi harga setiap gacha:

```
int gacha_cost = 160;
```

Lalu for loop dilakukan dengan kondisi seperti berikut agar berjalan sampai saldo tidak memadai:

```c
for(int balance = 79000; balance / gacha_cost > 0; balance -= gacha_cost){...}
```

Selanjutnya properti `rarity` dan `name` diekstrak dengan mengikuti tutorial dari link [https://progur.com/2018/12/how-to-parse-json-in-c.html] yang diterapkan menjadi:

```c
FILE *fptr;
char buffer[5192];

struct json_object *parsed_json;
struct json_object *gacha_name;
struct json_object *gacha_rarity;

fptr = fopen(gacha_file, "r");
fread(buffer, 5192, 1, fptr);
fclose(fptr);

parsed_json = json_tokener_parse(buffer);
json_object_object_get_ex(parsed_json, "name", &gacha_name);
json_object_object_get_ex(parsed_json, "rarity", &gacha_rarity);
```

Semua informasi tersebut diprint ke dalam file yang sesuai dengan perintah:

```c
print_to_file(curr_file, n, json_object_get_string(gacha_rarity), json_object_get_string(gacha_name), balance-gacha_cost);
```

- `e.` Agar gacha hanya berjalan pada waktu yang ditentukan yaitu 30 Maret 04:44, digunakan library `<time.h>` untuk mendapatkan waktu pada saat program dijalankan, yaitu seperti berikut:

```
time(&ttime);
time_struct = gmtime(&ttime);

month = time_struct->tm_mon;
date = time_struct->tm_mday;
hour = time_struct->tm_hour + 7;
minute = time_struct->tm_min;
second = time_struct->tm_sec;
```

Untuk mengecek apakah saat ini 30 Maret 04:44, digunakan kondisi if berikut:

```c
if(!(date-30) && !(month-2) && !(hour-4) && !(minute-44)){ 
	//implementasi gacha 
}
```

Untuk mengecek apakah sudah bisa zip hasil gacha dan menghapus semua file digunakan kondisi if berikut:

```c
if(!(date-30) && !(month-2) && !(hour-7) && !(minute-44)){
	//zip dan hapus
}
```

### Dokumentasi
![isi folder gacha_gacha](Dokumentasi/Soal1/folder_agg_gacha.png)*isi folder gacha_gacha*
![contoh isi folder gacha](Dokumentasi/Soal1/folder_gacha.png)*contoh isi folder gacha*
![contoh isi file gacha](Dokumentasi/Soal1/file_gacha.png)*contoh isi file gacha*


### Kesulitan
Pembuatan dan debugging proses daemon cukup sulit bagi praktikan. Penggunaan fungsi `exec()` dan variasinya juga perlu dipahami ciri-cirinya karena tidak sepenuhnya sama persis dengan terminal.

## Soal 2
Japrun bekerja di sebuah perusahaan dibidang review industri perfilman, karena kondisi saat ini sedang pandemi Covid-19, dia mendapatkan sebuah proyek untuk mencari drama korea yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review, tetapi didalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun merasa kesulitan untuk melakukan pekerjaannya secara manual, kamu sebagai programmer diminta Japrun untuk menyelesaikan pekerjaannya.

- `a.` Hal pertama yang perlu dilakukan oleh program adalah mengextract zip yang diberikan ke dalam folder `/home/[user]/shift2/drakor`. Karena atasan Japrun teledor, dalam zip tersebut bisa berisi folder-folder yang tidak penting, maka program harus bisa membedakan file dan folder sehingga dapat memproses file yang seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan.
- `b.` Poster drama korea perlu dikategorikan sesuai jenisnya, maka program harus membuat folder untuk setiap jenis drama korea yang ada dalam zip. Karena kamu tidak mungkin memeriksa satu-persatu manual, maka program harus membuatkan folder-folder yang dibutuhkan sesuai dengan isi zip.
**Contoh:** Jenis drama korea romance akan disimpan dalam `/drakor/romance`, jenis drama korea action akan disimpan dalam `/drakor/action` , dan seterusnya.
- `c.` Setelah folder kategori berhasil dibuat, program akan memindahkan poster ke folder dengan kategori yang sesuai dan di rename dengan nama.
**Contoh:** `/drakor/romance/start-up.png`
- `d.` Karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus di pindah ke masing-masing kategori yang sesuai. 
**Contoh:** foto dengan nama `start-up;2020;romance_the-k2;2016;action.png` dipindah ke folder `/drakor/romance/start-up.png` dan `/drakor/action/the-k2.png`.
- `e.` Di setiap folder kategori drama korea buatlah sebuah file **data.txt** yang berisi nama dan tahun rilis semua drama korea dalam folder tersebut, jangan lupa untuk sorting list serial di file ini berdasarkan tahun rilis (_Ascending_). **Format harus sesuai contoh dibawah ini.**
```
kategori : romance

nama : start-up
rilis  : tahun 2020

nama : twenty-one-twenty-five
rilis  : tahun 2022

```
`Note:`<br>
-	File zip berada dalam drive modul shift ini bernama `drakor.zip`<br>
-	File yang penting hanyalah berbentuk `.png`<br>
-	Setiap foto poster disimpan sebagai nama foto dengan format `[nama]:[tahun rilis]:[kategori]`. Jika terdapat lebih dari satu drama dalam poster, dipisahkan menggunakan underscore(_)<br>
-	Tidak boleh menggunakan fungsi `system()`, `mkdir()`, dan `rename()` yang tersedia di bahasa C<br>
-	Gunakan bahasa pemrograman C (Tidak boleh yang lain)<br>
-	Folder shift2, drakor, dan kategori dibuatkan oleh program (Tidak Manual)<br>
-	[user] menyesuaikan nama user linux di os anda<br>

### Penyelesaian
- `a.` Sebelum unzip, perlu dibuat direktori tujuan dimana pada permasalahan ini direktori tujuannya adalah `/home/azzuraf/shift2/drakor`.
```c
char *arg[] = {"mkdir","-p","/home/azzuraf/shift2/drakor", NULL};
execv("/usr/bin/mkdir", arg);
```
Setelah itu, karena download zip telah dilakukan secara mandiri oleh user, maka akan langsung dilakukan unzip. Pada saat unzip ini juga dilakukan seleksi pada file, dimana hanya file yang berbentuk `.png` yang akan di_pass_ ke folder tujuan. Tidak semua file maupun folder yang terdapat pada zip asal akan diunzip
```c
while((wait(&status)) > 0);
child_id = fork();

if(child_id == 0){
	char *argv[] = {"unzip","/home/azzuraf/Downloads/drakor.zip", "*.png","-d", "/home/azzuraf/shift2/drakor",NULL};
	execv("/usr/bin/unzip", argv);
}
```
Untuk menyelesaikan permasalahan poin `b.`, `c.`, dan `d.` akan dilakukan penyetoran list file terlebih dahulu.
```c
size_t listFile(const char *path, char ***ls){
	size_t count = 0;
	size_t length = 0;
	DIR *dp = NULL;
	struct dirent *ep = NULL;

	dp = opendir(path);
	if(NULL == dp){
		fprintf(stderr, "No such directory %s", path);
		return 0;
	}

	*ls = NULL;
	ep = readdir(dp);
	while(NULL != ep){
		count++;
		ep = readdir(dp);
	}

	rewinddir(dp);
	*ls = calloc(count, sizeof(char *));

	count = 0;
	ep = readdir(dp);
	while(NULL != ep){
		if(!strcmp(ep->d_name, ".") == 0 || !strcmp(ep->d_name, "..") == 0){
			(*ls)[count++] = strdup(ep->d_name);
                        ep = readdir(dp);
		}
		else{
			break;
		}
	}
	closedir(dp);
	return count;
}
```
> -  Function ini untuk melakukan penyetoran list file ke dalam array sekaligus menghapus semua isi selain file `.png` serta function bertipe data `size_t`. Function kami ambil referensi dari [sini](https://stackoverflow.com/questions/11291154/save-file-listing-into-array-or-something-else-c/11291863#11291863).
> - Function menerima argumen berupa directory files tadi dan &files bertipe char double pointer.
> - Pada Function, directory diterima oleh parameter path dan dimasukkan ke variabel `dp`. Jika `dp` NULL maka dilakukan pengembalian tidak ada directory. Argument kedua merupakan pointer files yang diterima oleh variabel `ls`. Nantinya `ls` ini sebagai return parameter yang berisi list file pada directory `dp`.
> - Selanjutnya terdapat variable temporary `ep = readdir(dp);` untuk melakukan perhitungan jumlah file pada directory dp dengan perulangan `while (NULL != ep)` dengan isi `count++`. Variable `count` nantinya untuk mengalokasikan jumlah memory array `l`s serta sebagai return value.
> - Dilakukan perulangan yang sama kembali, namun kali ini untuk mengambil list file. Diberikan conditional `if (!strcmp(ep->d_name, ".") == 0 && !strcmp(ep->d_name, "..") == 0)` untuk menghindari file `.` dan `..`.
> - Pada kondisional kedua tersebut, pada cabang bernilai True melakukan penyetoran nama file `(*ls)[count++] = strdup(ep->d_name);`. 
> - Terakhir dilakukan return value `count` serta didapatkan list file pada `files` yang direturn dari parameter.

- `b.` Dibuat fungsi dtmsplit untuk memisahkan nama file menggunakan delimiter.
```c
int dtmsplit(char *str, const char *delim, char ***array, int *length){
	int i=0;
	char *token;
	char **res = (char **)malloc(0 * sizeof(char *));

	//get first token
	token = strtok(str, delim);
	while(token != NULL){
		res = (char **)realloc(res, (i+1) * sizeof(char *));
		res[i] = token;
		i++;
		token = strtok(NULL, delim);
	}
	*array = res;
	*length = i;
	return 1;
}
```
> - Pertama dilakukan iterasi dari dari files dari 0 hingga `count`. Iterasi dilakukan kepada tiap nilai dari files atau tiap `files[i]`. Selanjutnya adalah pengambilan drakor pada nama file serta pemisahan drakor yang memiliki 2 judul drakor dalam 1 file.
> - Untuk pengambilan drakor, replace nama file 4 character terakhir dengan `\0` atau `files[i][strlen(files[i]) - 4] = '\0';`.
> - Selanjutnya dilakukan pemisahan str nama file dengan delimiter `_` sehingga didapatkan drakor tiap individu yang mana disimpan dalam variabel `indivDrakor` atau dengan syntax `dtmsplit(files[i], "_", &indivDrakor, &countTok);`. `countTok` untuk menghitung jumlah drakor individu setelah dilakukan pemisahan.
> - Selanjutnya tiap individu drakor dilakukan iterasi untuk diambil tiap nilai nama file dengan delimiter `;` (bila tidak gabungan, diiterasi sekali) atau dengan syntax `d = dtmsplit(temp, ";", &drakorAtr, &drakorAtrTok);`.Hasil pemisahan disimpan dalam variable `drakorAtr`.
> - Di dalam perulangan yang sama, dilakukan pemanggilan `child_id_mkdir = fork();`. Pada child process dilakukan pembuatan folder dengan `execv("/usr/bin/mkdir", {"mkdir", "-p", genre, NULL});` dengan nilai genre adalah hasil dari `sprintf(genre, "/home/azzuraf/shift2/drakor/%s", drakorAtr[2]);`. drakorAtr[2] adalah data genrenya. tanda -p pada mkdir untuk membatalkan pembuatan folder bila nama folder sudah ada.
```c
count = listFile("/home/azzuraf/shift2/drakor", &files);

for(int i=0; i < count; i++){
    char fileUntouched[70];
    strcpy(fileUntouched, files[i]);

    files[i][strlen(files[i]) - 4] = '\0';

    int c = 0;
    int countTok = 0;
    char **indivDrakor = NULL;

	c = dtmsplit(files[i], "_", &indivDrakor, &countTok);

    for(int j=0; j < countTok; j++){
    int d = 0;
    int drakorAtrTok = 0;
    char **drakorAtr = NULL;

    char temp[100];
    strcpy(temp, indivDrakor[j]);

    d = dtmsplit(temp, ";", &drakorAtr, &drakorAtrTok);

	char genre[100];
    sprintf(genre, "/home/azzuraf/shift2/drakor/%s", drakorAtr[2]);
               	              	
    int child_id_mkdir;
	int status_mkdir;

    child_id_mkdir = fork();

	if(child_id_mkdir == 0){
			if(strcmp(drakorAtr[2], "(null)") != 0){
                        char *arg[] = {"mkdir", "-p",genre, NULL};
                        execv("/usr/bin/mkdir", arg);
			}
    }
```

- `c.` dan `d.`
> - Untuk menyelesaikan permasalahan ini, dilakukan perulangan lanjutan dari soal 2b. Setelah selesai pembuatan folder pada child process, dilanjutkan pemindahan file pada parent process.
> - Pertama dilakukan pemanggilan  `child_id_cp = fork();` untuk melakukan pemindahan.
> - Selanjutnya akan dilakukan pemasukan nilai nama file ke variable temporary dengan `strcpy(filesUntouched, files[i]);`. Variable `filesUntouched` dimasukkan ke variable `fromFile` sebagai file asal dengan `sprintf(fromFile, "%s", filesUntouched);`. Adapun variable `toFile` sebagai destinasi file dengan `sprintf(toFile, "../drakor/%s/%s.png", drakorAtr[2], drakorAtr[0]);`. Kembali lagi, drakorAtr[2]  adalah genre dan drakorAtr[0] adalah judul drakor.
> - Terakhir, kita lakukan copy file dengan `execv("/usr/bin/cp", {"cp", fromFile, toFile, NULL});`.
```c
else{
		while ((wait(&status_mkdir) > 0));

		child_id = fork();

		if(child_id == 0){
			char dataPath[100];
			sprintf(dataPath, "/home/azzuraf/shift2/drakor/%s/data.txt", drakorAtr[2]);
							
			char name[100];
			sprintf(name,"%s", drakorAtr[0]);
			char year[10];
			sprintf(year,"%s", drakorAtr[1]);
							
			f = fopen(dataPath, "a");
			fprintf(f, "%s\n", name);
                fprintf(f, "%s\n", year);
                fclose(f);
							
			char fromFile[100], toFile[100];
			sprintf(fromFile, "%s", fileUntouched);
			sprintf(toFile, "../drakor/%s/%s.png", drakorAtr[2], drakorAtr[0]);

			chdir("/home/azzuraf/shift2/drakor/");

			char *arg[] = {"cp", fromFile, toFile, NULL};
			execv("/bin/cp", arg);
		}
}
```
- `e.` Jika dilihat dalam penyelesaian poin `c.` dan `d.`, dilakukan print nama file dan juga tahun di data.txt dengan folder sesuai genre dari masing-masing drakor. Dibuat file sekaligus melakukan print, apabila file sudah ada maka akan langsung menambahkan print dengan menggunakan syntax `f = fopen(dataPath, "a");` dimana `f` merupakan `FILE *f;` dan `datapath` merupakan hasil dari `sprintf(dataPath, "/home/azzuraf/shift2/drakor/%s/data.txt", drakorAtr[2]);` yang mana `drakorAtr[2]` merupakan data genre drakor.
```c
char dataPath[100];
sprintf(dataPath, "/home/azzuraf/shift2/drakor/%s/data.txt", drakorAtr[2]);

f = fopen(dataPath, "a");
fprintf(f, "%s\n", name);
fprintf(f, "%s\n", year);
fclose(f);
```
> - Dilakukan pembacaan file `data.txt` dari setiap direktori _branch_ yang ada di dalam direktori `/home/azzuraf/shift2/drakor`.
> - Pertama-tama, data yang ada di dalam `data.txt` akan dimasukkan ke dalam struct `drk` dengan menggunakan fungsi `fscanf`. Baris ganjil akan dimasukkan sebagai judul drakor sedangkan baris genap akan dimasukkan sebagai tahun rilis pada struct.
> - Selanjutnya dilakukan **bubble sorting** tahun rilis dari setiap drakor.
> - Terakhir akan dilakukan print dari seluruh data yang ada pada struct yang sudah di sort dengan melakukan perulangan.
```c
dp = opendir(path);
if (dp != NULL){
   while((ep = readdir(dp))){
        if (ep->d_type == 4){
            if (strstr(ep->d_name, "..") != NULL) continue;
            if (strstr(ep->d_name, ".") != NULL) continue;
            char dataPath[100];
            char ch;
            char name[100];
            char year[100];
            strcpy(dataPath, path);
            strcat(dataPath, "/");
            strcat(dataPath, ep->d_name);
            strcat(dataPath, "/data.txt");

            FILE *readFile;
            readFile = fopen(dataPath, "r");

            int index = 0;
            int line = 1; 
            while(!feof(readFile)){
                if (line%2 == 1){
                    fscanf(readFile, "%s", drk[index].name);
                }
                else{
                    fscanf(readFile, "%s", drk[index].year);
                    index++;
                }
                line++;
            }

            for (int i = 0; i<index-1; i++){
                for (int j = 0; j<index-i-1 ; j++){
                    int y1 = atoi(drk[j].year);
                    int y2 = atoi(drk[j+1].year);
                    if (y1 > y2){
                        char temp[100];
                        strcpy(temp, drk[j].year);
                        strcpy(drk[j].year, drk[j+1].year);
                        strcpy(drk[j+1].year, temp);

                        strcpy(temp, drk[j].name);
                        strcpy(drk[j].name, drk[j+1].name);
                        strcpy(drk[j+1].name, temp);
                    }

                }
            } 

            FILE *writeFile;
            writeFile = fopen(dataPath, "w");
            fclose(writeFile);
             writeFile = fopen(dataPath, "a");

            fprintf(writeFile, "Kategori: %s\n", ep->d_name);

            for (int i = 0; i<index; i++){
                fprintf(writeFile, "\nNama : %s\n", drk[i].name);
                fprintf(writeFile, "Tahun : %s\n", drk[i].year);
            }

            fclose(readFile);
            fclose(writeFile);
        }
    }
}
```
### Dokumentasi
![Isi akhir dari folder drakor](/Dokumentasi/Soal2/isi_folder_drakor.png) <br>
*Isi akhir folder drakor* <br>

![Contoh isi akhir dari folder genre](/Dokumentasi/Soal2/contoh_isi_folder_genre.png) <br>
*Contoh isi akhir dari folder genre* <br>

![Contoh isi data.txt](/Dokumentasi/Soal2/contoh_isi_data.png) <br>
*Contoh isi data.txt* <br>

### Kesulitan
Untuk menyelesaikan permasalahan ini, cukup banyak kesalahan yang terjadi pada saat penyelesaian, tetapi kesalahan cukup sulit untuk di-_debug_ sehingga cukup memakan waktu untuk mencari kesalahan.

## Soal 3
Conan adalah seorang detektif terkenal. Suatu hari, Conan menerima beberapa laporan tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang

- `a.` Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2 directory di `/home/[USER]/modul2` dengan nama **"darat"** lalu 3 detik kemudian membuat directory ke 2 dengan nama **"air"** <br>
- `b.` Kemudian program diminta dapat melakukan extract **"animal.zip"** di `/home/[USER]/modul2/` <br>
- `c.` Tidak hanya itu, hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder `/home/[USER]/modul2/darat` dan untuk hewan air dimasukkan ke folder `/home/[USER]/modul2/air`. Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.<br>
- `d.`Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air. Dikarenakan jumlah burung yang ada di kebun binatang terlalu banyak, maka pihak kebun binatang harus merelakannya sehingga conan harus menghapus semua burung yang ada di directory `/home/[USER]/modul2/darat`. Hewan burung ditandai dengan adanya **"bird"** pada nama file. <br>
- `e.` Terakhir, Conan harus membuat file **"list.txt"** di folder `/home/[USER]/modul2/air` dan membuat list semua hewan yang ada di directory `/home/[USER]/modul2/air` ke **"list.txt"** <br> dengan format **UID_[UID file permission]_Nama File.[jpg/png]** dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.
Contoh : **conan_rwx_hewan.png**

`Note:`

-  Tidak boleh memakai `system()` <br>
-  Tidak boleh memakai function C `mkdir()` ataupun `rename()`. <br>
-  Gunakan `exec` dan `fork` <br>
-  Direktori **"."** dan *".."** tidak termasuk

### Penyelesaian
Untuk menyelesaikan permasalahan soal nomor 3, pendekatan logika yang digunakan adalah dengan memanggil proses baru ketika proses *parent* sedang berjalan. Hal ini menjadi sebuah keharusan untuk dilakukan karena satu proses hanya bisa menangani satu kali eksekusi program, sehingga apabila tidak memanggil sebuah proses baru, maka program C akan berhenti pada eksekusi program terakhir dan eksekusi program lainnya tidak bisa dijalankan.

Untuk memanggil sebuah proses baru dapat menggunakan bantuan fungsi yang telah tersedia di C, yaitu `fork()`. Ketika fungsi `fork()` dipanggil, maka program akan memanggil 2 proses baru, yaitu proses *parent* dan proses *child*. Proses *child* akan dijalankan ketika ID prosesnya sama dengan 0, sedangkan proses *parent* berjalan ketika ID prosesnya lebih dari 0

Umumnya, pada setiap prosesor tidak ada aturan khusus mana yang harus dijalankan terlebih dahulu, antara proses *parent* atau *child*. Oleh karena itu, untuk mengantisipasi adanya program yang dieksekusi di luar urutan yang diharapkan, maka dibuat sebuah kondisi yang memaksa proses *parent* hanya bisa dijalankan setelah semua proses *child*-nya selesai. Kondisi tersebut dapat ditulis sebagai berikut.
```bash
while(wait(NULL)>0);
```

1. `a` <br>
> - Sebelum membuat directory, ada baiknya kita terlebih dahulu memeriksa apakah dirdctory tersebut sudah ada di sistem operasi kita atau belum. Untuk memeriksanya, kita dapat menggunakan bantuan fungsi `chdir()` untuk pindah ke directory yang ingin kita periksa. Apabila ternyata tidak bisa berpindah (fungsi mengembalikan nilai -1), maka directory tersebut akan dibuat.
> - Untuk membuat sebuah directory pada program C, kita dapat menggunakan salah satu fungsi yang tersedia yaitu `exec()`. Dalam kasus ini, fungsi `exec()` yang digunakan adalah `execvp()` karena terdapat path source yang perlu dicantumkan.
> - Perintah `mkdir` dapat kita sampaikan ke fungsi `execvp` dengan mengambilnya pada directory utilitas `/bin/mkdir`

```bash
if(chdir("/home/angela/modul2/darat") == -1) {
	char *argv[] = {"mkdir","-p", "/home/angela/modul2/darat", NULL};
	if(execvp("/bin/mkdir", argv) == -1){
		perror("mkdir darat failed!");
		exit(EXIT_FAILURE);
    }
}
```
> - Agar directory **"air"** dapat dibuat 3 detik setelah directory darat, maka sebelum fungsi `execvp()` untuk membuat directory air dijalankan, program C perlu diistirahatkan selama 3 detik terlebih dahulu. Untuk dapat melakukan hal tersebut, fungsi `sleep(3)` dipanggil.

```bash
sleep(3);
if(chdir("/home/angela/modul2/air") == -1) {
	char *argv[] = {"mkdir","-p", "/home/angela/modul2/air", NULL};
	if(execvp("/bin/mkdir", argv) == -1){
		perror("mkdir darat failed!");
		exit(EXIT_FAILURE);
    }
}
```

2. `b` <br>
> - Untuk melakukan extract atau *unzip* pada sebuah file juga dapat menggunakan bantuan fungsi `execvp()`. Namun kali ini, utilitas yang diakses adalah `/bin/unzip` dengan perintah `unzip`.
> - Agar folder hasil extract dapat langsung dipindahkan ke dircetory tujuan, maka perlu ditambahkan command pelengkap pada perintah `unzip`, yaitu `-d` dilanjutkan dengan directory tujuan.

```bash
char *argv[] = {"unzip", "/home/angela/animal2.zip", "-d", "/home/angela/modul2", NULL};
if(execvp("/bin/unzip", argv) == -1) {
	perror("unzip failed!");
	exit(EXIT_FAILURE);
	}
```

3. `c`
> - Untuk memindahkan file dengan keterangan tertentu ke folder sesuai dengan kategorinya, maka utilitas yang digunakan untuk fungsi `execvp()` pada kasus ini adalah `bin/bash` dengan command `bash`. Command bash ini juga akan dilengkapi dengan command `mv` atau *move* untuk memindahkan file tersebut ke folder tujuan
> - Sedangkan untuk filerisasi keterangan pada nama file agar dapat dikelompokkan sesuai dengan kategori, kita dapat menggunakan salah satu *syntax* filterisasi pada shell scripting, yaitu `*sebuah kata*`. Dengan menggunakan *syntax* tersebut, maka program akan mencari nama file yang mengandung **sebuah kata**.
> - Untuk memindahkan hewan darat, maka *syntax* filterisasi yang digunakan adalah `*darat*`. Sedangkan untuk hewan air, *syntax* filterisasi yang digunakan adalah `*air*`

```bash
char *argv[] = {"bash","-c", "mv /home/angela/modul2/animal/*darat* /home/angela/modul2/darat", NULL};
if(execvp("/usr/bin/bash", argv) == -1) {
	perror("move failed!");
	exit(EXIT_FAILURE);
	}
```

```bash
char *argv[] = {"bash","-c", "mv /home/angela/modul2/animal/*air* /home/angela/modul2/air", NULL};
if(execvp("/usr/bin/bash", argv) == -1) {
	perror("move failed!");
	exit(EXIT_FAILURE);
	}
```
> - Pada kasus ini, dibandingkan langsung menggunakan utilitas `/bin/mv` dengan command `mv`, hasil yang ditampilkan akan lebih tepat jika menggunakan command `bash`. Hal ini disebabkan karena apabila kita menggunakan command `mv`, maka *syntax* filterisasi `*sebuah kata*` akan memiliki konsep yang berbeda. Jika menggunakan command `mv` dengan filterisasi `*sebuah kata*`, maka program hanya akan menampilkan file yang secara literal memiliki nama **`*sebuah kata*`**, bukan file yang mengandung kata **"sebuah kata"** di dalam nama file tersebut.
> - Pada directory `animal` yang berisi file-file hasil extract dari file `animal.zip` sebelumnya, setelah dilakukan pemindahan file ke directory lainnya sesuai dengan kategori, maka akan tersisa file-file tanpa keterangan. Dengan kata lain, file yang tersisa pada directory `animal` perlu dihapus. Untuk menghapus file-file tanpa keterangan tersebut bersamaan dengan directory `animal`, maka dapat digunakan fungsi `execvp()` dengan mengakses utilitas `/bin/rm` dan dilengkapi dengan command `rm`. Agar semua file dan foldernya ikut terhapus, maka ditambahkan perintah pelengkap yaitu `-rf`

```bash
char *argv[] = {"rm", "-rf", "/home/angela/modul2/animal", NULL};
if(execvp("/usr/bin/rm",argv) == -1) {
	perror("remove failed!");
	exit(EXIT_FAILURE);
	}
```

4. `d`
> - Untuk menghapus file yang mengandung kata **"bird"** pada namanya, kita dapat menggunakan kembali *syntax* filterisasi yang sudah digunakan pada nomor sebelumnya, yaitu `*bird*`.
> - Karena *syntax* filterisasi ini hanya akan bersifat valid jika digunakan pada command `bash`, maka utilitas yang akan diakses adalah `/bin/bash` dengan command tambahan `rm` untuk menghapus file yang memenuhi kondisi.
```bash
char *argv[] = {"bash", "-c", "rm /home/angela/modul2/darat/*bird*", NULL};
if(execvp("/usr/bin/bash", argv) == -1) {
	perror("remove failed!");
	exit(EXIT_FAILURE);
	}
```

5. `e`
> - Untuk membuat sebuah file `.txt` pada directory kita, maka kita dapat membuat dan menggunakan sebuah variabel dengan tipe data **FILE** dan memberikan akses permission berupa *write* atau **w**.
> - Setelah itu, untuk menuliskan semua nama file yang ada di directory air, maka kita perlu melakukan *traverse* ke semua isi dari directory dengan path `/home/[USER]/modul2/air`
> - Setiap menemukan sebuah file baru, maka nama file tersebut akan dicatat ke dalam file `list.txt` yang sudah kita buat sebelumnya dengan menggunakan fungsi `fprintf()`
> - Tidak hanya nama file saja yang perlu dicatat, melakinkan user dari file tersebut dan akses permissionnya. Untuk mendapatkan user dari file, kita dapat menggunakan salah satu fungsi pada C, yaitu `getpwuid()`. Sedangkan, untuk mendapatkan akses permission user pada file tersebut dapat menggunakan bantuan sebuah variabel `fileStat` dengan tipe data **struct** `stat`.
> - Karena akses permission user ada 3 jenis, yaitu read, write, dan execute, maka program variabel `fileStat` perlu melakukan pemeriksaan satu per satu terhadap setiap permission. Untuk memeriksa akses read, maka `fileStat` akan memastikan bahwa permission user memiliki mode `S_IRUSR`. Jika tidak, maka pada bagian read, bukan `r` yang ditampilkan, melainkan `-`. 
> - Sama halnya dengan write dan execute. Pada write, program akan memeriksa apakah permission user memiliki mode `S_IWUSR`. Sedangkan pada execute, mode yang diperiksa pada permission user adalah `S_IXUSR`
> - Karena pada catatan soal, directory **"."** dan **".."** tidak perlu dicatat, maka perlu dituliskan sebuah kondisi agar kedua directory tersebut tidak tercatat ke dalam file `list.txt`. Hal tersebut dapat dilakukan dengan melakukan perbandingan **string** atau menggunakan fungsi `strcmp()`.
```bash
FILE *f = fopen("list.txt", "w");
if (f == NULL) {
	printf("Error opening file!\n");
	exit(1);
	}

DIR *directory;
struct dirent* dp;
struct stat fileStat;
struct passwd *pw = getpwuid(fileStat.st_uid);
char fullpath[256];

if((directory=opendir("/home/angela/modul2/air")) == NULL) {
	perror("Error in opening directory");
	exit(-1);
	}

while ((dp = readdir(directory)) != 0){
	strcpy (fullpath, "/home/angela/modul2/air");
	strcat (fullpath, "/");
	strcat (fullpath, dp->d_name);
		if (!stat(fullpath, &fileStat)) {
			if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") !=0) {
				fprintf(f,"%s_", pw->pw_name);
				fprintf(f,"%s", (fileStat.st_mode & S_IRUSR) ? "r" : "-");
				fprintf(f,"%s", (fileStat.st_mode & S_IWUSR) ? "w" : "-");
				fprintf(f,"%s_", (fileStat.st_mode & S_IXUSR) ? "x" : "-");
				fprintf(f,"%s",dp->d_name);
				fprintf(f,"\n");
				}
		}
		else {
			perror("Error in stat");
		}
	}
closedir(directory);
```
### Dokumentasi

![Isi akhir dari folder darat](/Dokumentasi/Soal3/isi_darat.png) <br>
*Isi akhir folder darat* <br>

![Isi akhir dari folder air](/Dokumentasi/Soal3/isi_air.png) <br>
*Isi akhir folder air`* <br>

![Isi dari file `list.txt`](/Dokumentasi/Soal3/isi_list.png) <br>
*Isi dari file `list.txt`* <br>

### Kendala
Pada saat praktikum, penulisan nama-nama file ke file `list.txt` masih terdapat kesalahan minor. Hal ini disebabkan karena program menampilkan `"\n"` di luar kondisi yang membatasi penampilan directory **"."** dan **".."**. Sehingga, ketika program menemui kedua directory tersebut, pada file `list.txt` akan menampilkan baris kosong. Pada saat praktikum, terdapat dua baris kosong pada file `list.txt`.
