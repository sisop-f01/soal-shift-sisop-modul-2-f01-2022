#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <stdlib.h>
#include <wait.h>
#include <string.h>
#include <pwd.h>

int main () {
	pid_t child_id;
	int status;

	child_id = fork();

	if (child_id < 0) {
	  exit(EXIT_FAILURE);}
	if (child_id==0) {
		if(chdir("/home/angela/modul2/darat") == -1) {
			char *argv[] = {"mkdir","-p", "/home/angela/modul2/darat", NULL};
			if(execvp("/bin/mkdir", argv) == -1){
				  perror("mkdir darat failed!");
		  		  exit(EXIT_FAILURE);
			}
		}
	}

	else {
		child_id = fork();
			if(child_id < 0) {
				exit(EXIT_FAILURE);}
			if(child_id==0) {
				char *argv[] = {"unzip", "/home/angela/animal2.zip", "-d", "/home/angela/modul2", NULL};
				if(execvp("/bin/unzip", argv) == -1) {
					perror("unzip failed!");
					exit(EXIT_FAILURE);
				}
			}
			else {
				while(wait(NULL) > 0);
				child_id = fork();
				if(child_id < 0) {
					exit(EXIT_FAILURE);}
				if(child_id == 0) {
					char *argv[] = {"bash","-c", "mv /home/angela/modul2/animal/*darat* /home/angela/modul2/darat", NULL};
						if(execvp("/usr/bin/bash", argv) == -1) {
							perror("move failed!");
							exit(EXIT_FAILURE);
						}
				}
				else {
					while(wait(NULL)>0);
					sleep(3);
					child_id = fork();
					if(child_id<0) {
						exit(EXIT_FAILURE);}
					if(child_id==0) {
						while((wait(&status))>0);
						if (chdir("/home/angela/modul2/air") == -1) {
							char *argc[] = {"mkdir", "-p", "/home/angela/modul2/air",NULL};
							if(execvp("/bin/mkdir", argc) == -1) {
								perror("mkdir air failed!");
								exit(EXIT_FAILURE);
							}
						}
					}
					else {
						while(wait(NULL)>0);
						child_id = fork();
						if(child_id<0) {
							exit(EXIT_FAILURE);}
						if(child_id==0) {
						char *argv[] = {"bash","-c", "mv /home/angela/modul2/animal/*air* /home/angela/modul2/air", NULL};
							if(execvp("/usr/bin/bash", argv) == -1) {
								perror("move failed!");
								exit(EXIT_FAILURE);
							}
						}
						else {
							while(wait(NULL)>0);
							child_id = fork();
							if(child_id < 0) {
								exit(EXIT_FAILURE);}
							if(child_id==0) {
								char *argv[] = {"rm", "-rf", "/home/angela/modul2/animal", NULL};
								if(execvp("/usr/bin/rm",argv) == -1) {
									perror("remove failed!");
									exit(EXIT_FAILURE);
								}
							}
							else {
								while(wait(NULL)>0);
								child_id = fork();
								if(child_id < 0) {
									exit(EXIT_FAILURE);}
								if(child_id == 0) {
								char *argv[] = {"bash", "-c", "rm /home/angela/modul2/darat/*bird*", NULL};
									if(execvp("/usr/bin/bash", argv) == -1) {
										perror("remove failed!");
										exit(EXIT_FAILURE);
									}
								}
								else {
									while(wait(NULL)>0);
									child_id = fork();
									if(child_id < 0) {
									exit(EXIT_FAILURE);}
									if(child_id == 0) {
										FILE *f = fopen("list.txt", "w");
										if (f == NULL)
										{
											printf("Error opening file!\n");
											exit(1);
										}
										DIR *directory;
										struct dirent* dp;
										struct stat fileStat;
										struct passwd *pw = getpwuid(fileStat.st_uid);
										char fullpath[256];

										if((directory=opendir("/home/angela/modul2/air")) == NULL)
										{
											perror("Error in opening directory");
											exit(-1);
										}
										while ((dp = readdir(directory)) != 0)
										{
												strcpy (fullpath, "/home/angela/modul2/air");
												strcat (fullpath, "/");
												strcat (fullpath, dp->d_name);
											if (!stat(fullpath, &fileStat))
											{
												 if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") !=0)
												{
													fprintf(f,"%s_", pw->pw_name);
													fprintf(f,"%s", (fileStat.st_mode & S_IRUSR) ? "r" : "-");
													fprintf(f,"%s", (fileStat.st_mode & S_IWUSR) ? "w" : "-");
													fprintf(f,"%s_", (fileStat.st_mode & S_IXUSR) ? "x" : "-");
													fprintf(f,"%s",dp->d_name);

											fprintf(f,"\n");
												}
											}
											else {
												perror("Error in stat");
											}
										}
										closedir(directory);
									}
									else {
									while(wait(NULL)>0);
									char* argv[] = {"mv", "/home/angela/list.txt", "/home/angela/modul2/air"};
										if(execvp ("/usr/bin/mv",argv)==-1)
										{
											perror("move failed!");
											exit(EXIT_FAILURE);
										}
									}
								}
							}
						}
					}
				}
		}

	}
}
